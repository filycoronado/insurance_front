import { Component, OnInit } from '@angular/core';
import { CustomerService } from 'app/Services/CustomerService';
import { LoginService } from 'app/Services/LoginService';


@Component({
  selector: 'app-clients-main-page',
  templateUrl: './clients-main-page.component.html',
  styleUrls: ['./clients-main-page.component.scss']
})
export class ClientsMainPageComponent implements OnInit {


  private apiservice: CustomerService;
  private LoginService: LoginService;
  public receivedChildMessage: string;

  constructor(_apiservice: CustomerService, _loginService: LoginService) {
    this.apiservice = _apiservice;
    this.LoginService = _loginService;
  }


  entities: any[] = [
    {
      id: 1,
      name: 'Alex H',
      city: 'Guadalajara'
    },
    {
      id: 2,
      name: 'Rodo Gonzales',
      city: 'Hermosillo'
    },
    {
      id: 4,
      name: 'Juan C',
      city: 'Obregon'
    },
    {
      id: 5,
      name: 'Juan O',
      city: 'Hermosillo'
    },
    {
      id: 6,
      name: 'Edson B',
      city: 'Guaymas'
    }
  ];

  getAll() {
    this.apiservice.getAll().subscribe(response => {
      alert(response['message']);
    });
  }

  ngOnInit() {
    if (!this.LoginService.CheckLogin()) {
      this.getAll();
    }
  }

  getMessage(message: string) {
    this.receivedChildMessage = message;
    alert(this.receivedChildMessage);
  }

}
