import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Agent } from 'app/Models/Agent';
import { AgentService } from 'app/Services/AgentService';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  private apiservice: AgentService;
  constructor(private router: Router,_apiservice: AgentService, public formBuilder: FormBuilder) {
    this.apiservice = _apiservice;
  }
  data: Agent;
  LoginForm: FormGroup;
  login(): void {
    this.apiservice.Login(this.data).then(res => {
      this.data = res.data;
      if (res.status == "success") {
        alert(res.message);
        localStorage.setItem("login_insurance", JSON.stringify(this.data));
        this.router.navigate(['/', 'dashboard']);
      }
    }, (err) => {

    });

  }
  initForm() {
    this.LoginForm = this.formBuilder.group({
      username: ['', [Validators.required]],
      password: ['', [Validators.required]]

    })
  }
  get getControl() {
    return this.LoginForm.controls;
  }
  getValuesForm() {
    this.data = new Agent();
    this.data.username = this.LoginForm.get('username').value;
    this.data.password = this.LoginForm.get('password').value;
  }

  onSubmit() {
    if (this.LoginForm.valid) {
      this.getValuesForm();
      this.login();
    } else {
      alert("Error");
    }
  }
  ngOnInit() {
    this.initForm();

  }

}
