import { Component, OnInit } from '@angular/core';
import { City } from 'app/Models/City';
import { CityService } from 'app/Services/CityService';
import { LoginService } from 'app/Services/LoginService';

@Component({
  selector: 'app-new-policy',
  templateUrl: './new-policy.component.html',
  styleUrls: ['./new-policy.component.scss']
})
export class NewPolicyComponent implements OnInit {

  private apiservice: CityService;
  data: City;
  constructor(_apiservice: CityService) {
    this.apiservice = _apiservice;
  }
  getAll() {

    this.apiservice.GetAllCities().then(res => {
      if (res.status == "success") {
        this.data = res.data;
        alert(res.message);
      }
    }, (err) => {

    });

  }

  ngOnInit() {
    this.getAll();
  }

}
