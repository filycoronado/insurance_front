import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BaseService } from './BaseService';



@Injectable({
    providedIn: 'root'
})
export class CustomerService extends BaseService {
    constructor(public  http: HttpClient) {
        super(http, "customer");
    }


}
