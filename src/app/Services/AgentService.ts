import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BaseService } from './BaseService';
import { Iagent } from './Iagent';
import { Observable } from 'rxjs';
import { resolve } from '@angular/compiler-cli/src/ngtsc/file_system';

@Injectable({
    providedIn: 'root'
})
export class AgentService extends BaseService implements Iagent {
    constructor(public http: HttpClient) {
        super(http, "agent");
    }

    Login2(data: any): Promise<any> {
        /* let data;
         this.http.post<any>(this.localurl + "/login", { data: any }).subscribe(res => {
             data = res;
         });
         return data;*/

        return new Promise((resolve, reject) => {
            return this.http.post<any>(this.localurl + "/login", { data: data }).subscribe((res) => {
                resolve(res);
            }, (err) => {
                reject(err);
            });
        });//promise


    }


    Login(data): Promise<any> {
        return new Promise((resolve, reject) => {
            return this.http.post<any>(this.localurl + '/login', { data }).subscribe((res) => {
                resolve(res);
            }, (err) => {
                reject(err);
            });
        });//promise
    }


}