import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BaseService } from './BaseService';
import {  Icity } from './Iagent';
import { Observable } from 'rxjs';
import { resolve } from '@angular/compiler-cli/src/ngtsc/file_system';

@Injectable({
    providedIn: 'root'
})
export class CityService extends BaseService implements Icity {
    constructor(public http: HttpClient) {
        super(http, "city");
    }

    GetAllCities(): Promise<any> {
        debugger;
        return new Promise((resolve, reject) => {
            return this.http.get<any>(this.localurl + '/getall' ).subscribe((res) => {
                resolve(res);
            }, (err) => {
                reject(err);
            });
        });//promise
    }


}