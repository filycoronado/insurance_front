import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { baseObject } from './Response';


export interface IBase {

    save(any): Observable<baseObject>;
    getAll(): Observable<Array<any>>;
}
