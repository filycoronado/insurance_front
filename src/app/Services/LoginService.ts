import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BaseService } from './BaseService';
import { Router } from '@angular/router';



@Injectable({
    providedIn: 'root'
})
export class LoginService extends BaseService {
    constructor(private router: Router, public http: HttpClient) {
        super(http, "customer");
    }

    public CheckLogin(): boolean {
        if (!localStorage.getItem('login_insurance')) {
            this.router.navigate(['/', 'login']);
            return true;
        }
        return false;
    }
}


