import { HttpClient } from "@angular/common/http";
import { environment } from "environments/environment";
import { Observable } from "rxjs";
import { IBase } from "./IBase";
import { baseObject } from "./Response";

export class BaseService implements IBase {
    public  localurl = environment.Apiurl;
    constructor(public http: HttpClient, private controller) {

        this.localurl += "" + this.controller;
    }
    save(any: any): Observable<baseObject> {
        throw new Error('Method not implemented.');
    }
    getAll(): Observable<any[]> {
        return this.http.get<any[]>(this.localurl + "/login");
    }



}