import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.scss']
})
export class SearchBarComponent implements OnInit {
  @Input() Title: string;
  @Output() messageToEmit = new EventEmitter<string>()
  name;
  last_name;
  phone;
  constructor() { }

  ngOnInit() {
  }

  sendMessageToParent() {
    this.messageToEmit.emit(this.name + "" + this.last_name + "" + this.phone)
  }
 

}
