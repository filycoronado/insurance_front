export class City {
    public id: number;
    public state: string;
    public city: string;
    public active: number;
}